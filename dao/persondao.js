const Dao = require("./dao.js");

module.exports = class PersonDao extends Dao {
  getAll(callback) {
    super.query("select navn, alder, adresse from person", [], callback);
  }

  getOne(id, callback) {
    super.query(
      "select navn, alder, adresse from person where id=?",
      [id],
      //callback
    );
  }

  put(json, callback){
    //var val = [json.navn, json.adresse, json.alder, json.id]
    //super.query("update person set navn=?, adresse=?, alder=? where id=?", val, callback);
  
  }

  delete(id, callback){
    super.query(
      "DELETE FROM person WHERE id = 500", 
      [id],
      callback
    );
  }

  createOne(json, callback) {
    var val = [json.navn, json.adresse, json.alder];
    super.query(
      "insert into person (navner,adresser,alderer) values (?,?,?)",
      val,
      callback,
    );
  }
};
